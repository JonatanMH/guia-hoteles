$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 3000
    });

    $('#Modalregistro').on('show.bs.modal', function (e) {
      console.log("Se abre el modal");
      $('#Btn-Modal-Registro').removeClass('btn-primary');
      $('#Btn-Modal-Registro').addClass('btn-outline-primary');
      $('#Btn-Modal-Registro').prop('disabled', true);


    });
    $('#Modalregistro').on('shown.bs.modal', function (e) {
      console.log("Se abrio el modal")
    });
    $('#Modalregistro').on('hide.bs.modal', function (e) {
      console.log("Se oculta el modal")
      $('#Btn-Modal-Registro').removeClass('btn-outline-primar');
      $('#Btn-Modal-Registro').addClass('btn-primary');
      $('#Btn-Modal-Registro').prop('disabled', false);
    });
    $('#Modalregistro').on('hidden.bs.modal', function (e) {
      console.log("Se oculto el modal")
    });


  });